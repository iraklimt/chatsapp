import http from './httpService';
import { apiEndpoint } from '@/config/config.json';

// We don't use async/await here, we will use it when we call to this function.

export const getUsers = () => {
  return http.get(apiEndpoint + '/users');
};

export const getUser = (id) => {
  return http.get(apiEndpoint + '/users/' + id);
};

export const updateUser = (user) => {
  return http.put(apiEndpoint + '/users/' + user.id, {
    user: user,
  });
};
