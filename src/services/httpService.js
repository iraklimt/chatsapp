import axios from 'axios';

axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
axios.defaults.headers.post['Content-Type'] =
  'application/x-www-form-urlencoded';
// Axios Interceptor.
axios.interceptors.response.use(
  null /* sucess */,
  /* error */ (error) => {
    const expectedError =
      error.response &&
      error.response.status >= 400 &&
      error.response.status < 500;
    if (!expectedError) {
      console.log('UNEXPECTED REQUEST ERROR: ', error);
    }
    return Promise.reject(error);
  }
);

// For this app we just need get and update users
export default {
  get: axios.get,
  put: axios.put,
  // post: axios.post,
  // delete: axios.delete,
};
