import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersist from 'vuex-persist';

const vuexPersist = new VuexPersist({
  key: 'app-data',
  storage: window.sessionStorage,
});

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    name: '',
    avatar: '',
    profiles: '',
  },
  mutations: {
    UPDATE_NAME(state, status) {
      state.name = status;
    },
    UPDATE_AVATAR(state, status) {
      state.avatar = status;
    },
    UPDATE_PROFILES(state, status) {
      state.profiles = status;
    },
  },
  actions: {
    updateName(context, name) {
      // firs char upeprcase
      let newName = name;
      if (name !== '') {
        newName = `Hello ${name.charAt(0).toUpperCase() + name.slice(1)}!`;
      } else {
        newName = '';
      }
      context.commit('UPDATE_NAME', newName);
    },

    updateAvatar(context, avatar) {
      context.commit('UPDATE_AVATAR', avatar);
    },

    updateProfiles(context, profile) {
      const profiles = [...context.state.profiles];
      profiles.splice(profiles.indexOf(profile), 1);
      profiles.push(profile);
      context.commit('UPDATE_PROFILES', profiles);
    },

    storeUsers(context, profiles) {
      context.commit('UPDATE_PROFILES', profiles);
    },
  },
  getters: {
    getProfile: (state) => (id) => state.profiles.find((p) => p.id === id),
  },

  // modules: {},
  plugins: [vuexPersist.plugin],
});
