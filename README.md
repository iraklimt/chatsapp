# chatsapp

## Preface

In order to make the app a little bit more funny and complete I added some features that improve the default behaviour, however, all the project requirements are met.

## Description

In the recent version, the app reliess on a RESTful API to consume the required data (users list) and makes asynchronous request calls behind the scene to get or update the users. The idea is to delegate the data management responsibility as maximum possible the to the front VueJS app, increasing so the speed and user experience.

The app has 4 pages that are described as follows:

- Home: Here the user can choose an avatar and type his name.
- Profiles: The user has a default list of profiles where he can find or filter the profiles by friends/not friends. He can view profile pages clicking over each profile.
- Profile: Here the user can interact with the profile. He can add/remove the profile as friend, add the profile to favorites or chat with the profile clicking over his picture.
- Chat: Here the target user can chat with the selected profile. After sending a message. the profile answers automatically.
- Navigation: The nav menu with back button to navigate between all pages.

## Arquitecture

The app is implemented using VueJS framework, including VueRouter, Vuex and VuexPersist libraries.

- Vuex: Manages the state and data flow between components.
- VueRouter: Is used to navigate between the different views.
- VuexPersist: This library is used to store the state in sessionStorage to meet the project requirements.

## Random profiles

The sample profiles names and pictures are taken from the TeamCMP webpage. Some data as cities or ages are assumed.

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```
